from time import sleep

from gpiozero import MCP3008
from oscpy.client import OSCClient

address = 'localhost'
port = 12345

client = OSCClient(address, port)

pot = MCP3008(0)
value = 0

while True:
    new_value = int(pot.value * 100)
    # on détecte si la valeur change pour ne pas envoyer
    # de message osc inutile
    if value != new_value:
        value = new_value
        print('pot value:', value)
        client.send_message(b'/neopixel/pos', [value/100])
    sleep(0.01)
