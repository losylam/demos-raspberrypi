from gpiozero import MCP3008
from time import sleep

pot = MCP3008(0)

while True:
    print(round(pot.value, 3))
    sleep(0.01)
