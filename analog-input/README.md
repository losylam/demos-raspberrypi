
# Entrée analogiques avec gpiozero et la puce MCP3008

## Ressources

- [Tutoriel détaillé](https://projects.raspberrypi.org/en/projects/physical-computing/13) sur le site "projets" de la fondation Raspbery PI.

## Installation

Pour tirer le meilleur parti de la puce, il faut utiliser un bus SPI, qui nécessite deux étapes de configuration :

- L'installation de la bibliothèque python spidev, à l'aide de la commande suivante :

```
sudo apt-get install python3-spidev python-spidev
```

 - L'activation du bus SPI dans la configuration de Raspberry PI :

 Menu Franboise > Préférences > Configuration du raspberry pi > Onglet "Interfaces"

