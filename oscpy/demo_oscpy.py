# Un programme qui envoie un message OSC lorsqu'on appuie sur un bouton
# et qui change l'état d'une led lorsqu'on reçoit un message osc

from gpiozero import LED, Button
from signal import pause
from oscpy.client import OSCClient
from oscpy.server import OSCThreadServer

led = LED(19)
button = Button(26)


#::::::::::::::::::::::::#
# CONGIGURATION SERVEUR  #
#::::::::::::::::::::::::#

# on ecoute sur le port 12345
ip = "0.0.0.0"
port = 12345

# fonction qui affiche le message reçu
def callback(*values):
    led.toggle()
    print("got message: {}".format(values))

# initialisatoin du server
osc = OSCThreadServer()
osc.listen(address=ip, port=port, default=True)
# lier le message b'message à la fonction callback
osc.bind(b'/ping', callback)


#::::::::::::::::::::::::#
# CONFIGURATION CLIENT   #
#::::::::::::::::::::::::#

# on envoie un message localement sur le port 12345
# (en théorie on voudra plutôt envoyer à une ou plusieurs machines distantes)
address = "localhost"
port = 12345

# initialisation du client
osc = OSCClient(address, port)

# envoie d'un message (contenant du texte et 4 nombres)
def send():
    print('send message')
    osc.send_message(b'/ping', [b'test', 20, 0, 10, 0.1])

button.when_pressed = send

pause()
