# Recevoir des message OSC avec oscpy

from signal import pause
from oscpy.server import OSCThreadServer

# On ecoute sur le port 12345
ip = "0.0.0.0"
port = 12345

server = OSCThreadServer()
server.listen(address=ip, port=port, default=True)

# On définit une fonction qui va être executé quand certains
# messages seront reçus (appelée arbitrairement callback_ping)
def callback_ping(*values):
    print("got message:", values)

# on lie l'adresse osc '/ping' à la fonction callback_ping
server.bind(b'/ping', callback_ping)

## Autre syntaxe utilisant des "décorateurs":
#@server.address('b/ping')
#def ping(*values):
#    print("got message:", values)

pause()
