# Communication OSC en python avec le module oscpy

## Ressources

- La documentation se trouve sur la page d'accueuil du [dépot github](https://github.com/kivy/oscpy) de la bibliothèque oscpy.

## Installation

Il faut installer la bibliothèque `oscpy` :
```
sudo pip install --break-system-install oscpy
```
