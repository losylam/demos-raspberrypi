# Envoyer un message OSC en python avec le module oscpy

from oscpy.client import OSCClient

# pour envoyer un message osc localement
# (le serveur, qui reçoit le message, est sur la même machine)
address = 'localhost'
port = 12345

osc = OSCClient(address, port)
osc.send_message(b'/ping', [b'bonjour'])

# le premier argument "b'/ping'" est l'"adresse" osc
# le deuxième argument contient une liste de données qui peuvent être du texte ascii,
# ou des nombres entiers ou flottants
osc.send_message(b'/ping', [b'bonjour', 0, 1.999])

# même si on ne veut pas envoyer de valeurs, il faut ajouter une liste vide
osc.send_message(b'/ping', [])

