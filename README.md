# Démos - Raspberry Pi

Quelques tutoriels pour faire des choses en python avec un raspberry pi

- Utilisation des broches avec [gpiozero](./gpiozero)
- Communication osc avec [oscpy](./oscpy)
- Utilisation des leds RGB type [neopixel](./neopixel)
- [Entrées analogiques](./analog-input) avec une puce MCP3008

## Ressources 

- [Tutoriel gpiozero](https://projects.raspberrypi.org/en/projects/physical-computing/1) sur le site de Raspberry pi
- [Documentation des capteurs](https://sensorkit.joy-it.net/fr/) du kit 40 capteurs Joyit
