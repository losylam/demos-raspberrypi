from signal import pause

import time
import math
import board
import neopixel

from oscpy.server import OSCThreadServer

server = OSCThreadServer()
server.listen("0.0.0.0", 12345, default=True)

pixel_pin = board.D21
num_pixels = 50

pixels = neopixel.NeoPixel(
    pixel_pin,
    num_pixels,
    brightness=0.05,
    auto_write=False,
    pixel_order=neopixel.GRB
)


def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos * 3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos * 3)
        g = 0
        b = int(pos * 3)
    else:
        pos -= 170
        r = 0
        g = int(pos * 3)
        b = int(255 - pos * 3)
    return (r, g, b)


@server.address(b'/neopixel/off')
def led_off():
    pixels.fill((0, 0, 0))
    pixels.show()
    print('led off')


@server.address(b'/neopixel/rgb')
def change_color_rgb(*values):
    if len(values) == 3:
        pixels.fill(values)
        pixels.show()
        print('set rgb: ', values)
    else:
        print('rgb failed:', values)


@server.address(b'/neopixel/hue')
def change_color_hue(*values):
    if len(values) == 1:
        hue = values[0]
        col = wheel(hue)
        pixels.fill(col)
        pixels.show()
        print('set hue:', hue)
    else:
        print('hue failed:', values)

@server.address(b'/neopixel/pos')
def change_pos(*values):
    print('set pos:', values)
    if len(values) == 1:
        pos = math.floor(values[0]*num_pixels)
        pos = min(num_pixels-1, pos)
        pixels.fill((0, 0, 0))
        pixels[pos] = (255, 0, 255)
        pixels.show()
        print('set pos:', pos)

pause()
