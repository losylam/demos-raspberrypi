# Neopixel - Leds RGB adressables

Les "neopixels" sont des leds RGB adressables, c'est-�-dire que l'on peut choisir la couleur de chacune des leds ind�pendament des autres. Elles sont cha�n�es et n'utilise qu'une broche de communication en plus de l'alimentation en 5V.

## Ressources

- Tutoriel [Adafruit](https://learn.adafruit.com/neopixels-on-raspberry-pi): Assez complet et d�taill� notamment en ce qui concerne le circuit � r�aliser.

## Installation

On doit installer plusieurs biblioth�ques logiciels en tant que super-utilisateur (en utilisant la pr�commande `sudo`). Avec python3.11, on ajoute �galement l'option `--break-system-packages`.

```bash
sudo pip3 install --break-system-packages rpi_ws281x adafruit-circuitpython-neopixel
sudo python3 -m pip install --break-system-packages --force-reinstall adafruit-blinka
```

## Ex�cution

Les droits "super-utilisateur" sont n�cessaires pour utiliser les bibioth�ques neopixels, on lancera donc le programme dans un terminal avec la pr�commande `sudo` :

```bash
sudo python demo_neopixel.py
```


## Remarques

- Les leds RGB consomment beaucoup de courant. Si elles sont aliment�es directement depuis une broche 5V du raspberry pi, il est conseill� de ne pas d�passer une dizaine de leds et de r�duire leur luminosit� au maximum.
- Dans le tutoriel [Adafruit](https://learn.adafruit.com/neopixels-on-raspberry-pi), c'est la broche `D18` qui est utilis�e. Comme celle-ci n�cessite de desactiver l'audio du raspberry pi, on utilisera la broche `D21`
- Les leds ws2811 ou les variantes qui sont utilis�es fonctionnent en 5V contrairement au raspberry qui est en 3.3V. Il peut donc �tre n�cessaire d'ajouter un circuit interm�diaire ("level shifter") pour adapter le niveau de la commande.
