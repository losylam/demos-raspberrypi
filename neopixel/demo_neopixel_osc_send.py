from oscpy.client import OSCClient
from time import sleep

osc = OSCClient(address="localhost", port=12345)

osc.send_message(b'/neopixel/rgb', [0, 255, 0])
sleep(1)
osc.send_message(b'/neopixel/rgb', [128, 0, 128])
sleep(1)
osc.send_message(b'/neopixel/rgb', [128, 0, 0])
sleep(1)
for i in range(250):
    osc.send_message(b'/neopixel/hue', [i])
    sleep(0.01)

osc.send_message(b'/neopixel/off', [])
