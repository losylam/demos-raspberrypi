from gpiozero import LED, Button

# Instantiation d'un objet LED connecté à la broche 19
# circuit: D19 ·--[+ led -]--[Résistance]--· gnd
led = LED(19)

# Instantiation d'un objet Button connecté à la broche 26
# circuit: D26 ·--[bouton]--· gnd
# (pour gpiozero, le bouton est appuyé quand il ferme le circuit
# entre la broche (D26) et la masse (GND))
button = Button(26)

button.when_pressed = led.on
button.when_released = led.off
