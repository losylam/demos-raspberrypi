# Gpiozero

`gpiozero` est une bibliothèque python pour communiquer avec les broches du Raspberry Pi.

## Ressources

- [Documentation officielle](https://gpiozero.readthedocs.io)
- [Documentation complète de l'objet LED](https://gpiozero.readthedocs.io/en/latest/api_output.html)
- [Documentation complète de l'objet Button](https://gpiozero.readthedocs.io/en/latest/api_input.html)

Il y a aussi un [tutoriel détaillé](https://projects.raspberrypi.org/en/projects/physical-computing/1) sur le site ressource de Raspberry PI.

## Installation

`gpiozero` est installé par défaut sur toutes les versions récentes de Rasbperry Pi OS.
